import React, { Component } from 'react';
import authService from './api-authorization/AuthorizeService'
import Autocom from './autocomplete'
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import Button from "@material-ui/core/Button";
import inputbar from './inputbar';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));


export class FetchData extends Component {
    static displayName = FetchData.name;
 //   static classes = useStyles();

    constructor(props) {
        super(props);
        this.state = { forecasts: [], loading: true };
    }

    componentDidMount() {
        this.populateWeatherData();

        this.DoTransaction();
    }

    static renderForecastsTable(forecasts) {
        return (
            <table className='table table-striped' aria-labelledby="tabelLabel">
                <thead>
                    <tr>
                        <th>User Name</th>
                        <th>Amount</th>
                        <th>Time</th>
                        <th>Succeed</th>
                    </tr>
                </thead>
                <tbody>
                    {forecasts.map(forecast =>
                        <tr key={forecast.transactionId}>
                            <td>{forecast.toName}</td>
                            <td>{forecast.amount}</td>
                            <td>{forecast.time}</td>
                            <td>{forecast.succeed ? "succeed" : "fail"}</td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : FetchData.renderForecastsTable(this.state.forecasts);
        
      //  const  classes = useStyles();
        return (
            <div>
                <h1 id="tabelLabel" >Transaction service</h1>
                <p>Test task</p>

                

                <form >
                   
              
                        <TextField
                            id="standard-textarea"
                            label="Transfer amount"
                            placeholder="Placeholder"
                            
                        />
                     
                        <Autocom />

                        <Button variant="contained" color="primary">
                            Send
                        </Button>
                   
                </form>
                
                {contents}
            </div>
        );
    }
   
    async DoTransaction() {
        const token = await authService.getAccessToken();

      
        axios.post(
            'transaction', // url
            {
                'username': "artemmail@gmail.com",
                'amount': 49 
            }, // data
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}` 
                },
                // `withCredentials` indicates whether or not cross-site Access-Control requests
                // should be made using credentials
                withCredentials: true // ����� ���� ����, � ����� ���� ���
            } // config
        )
            .then(function (response) {
                console.log(response); // ����� ���������� ����� ��� ����
            })
            .catch(function (error) {
                console.log(error);
            });
    }

  async populateWeatherData() {
    const token = await authService.getAccessToken();
    const response = await fetch('transaction', {
      headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
    });
    const data = await response.json();
    this.setState({ forecasts: data, loading: false });
  }
}
