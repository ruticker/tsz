﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.AspNetCore.Identity;
using TSZ.Models;
using TSZ.Data;

namespace TSZ.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("[controller]")]
    public class TransactionController : ControllerBase
    {
        private readonly ILogger<TransactionController> _logger;
        private readonly ApplicationDbContext _ctx;
        private readonly UserManager<ApplicationUser> _manager;

        public TransactionController

            (ApplicationDbContext ctx,
            UserManager<ApplicationUser> manager, IClientRequestParametersProvider clientRequestParametersProvider,
            ILogger<TransactionController> logger)
        {
            _ctx = ctx;
            _manager = manager;
            _logger = logger;
        }

        [HttpPost]
        public Transaction Post(Pay pay)
        {
          var username = _manager.FindByNameAsync("artemmail@gmail.com"/*User.Identity.Name*/).Result; 
          var acceptorname = _manager.FindByNameAsync(pay.username).Result;
          var res = _ctx.Transfer(username, acceptorname, pay.amount);
          return res;    
        }

        [HttpGet]
        public IEnumerable<TransactionList> Get()
        {
            return _ctx.Transactions.
                   Select(x => new TransactionList()
                   {
                       TransactionId = x.TransactionId,
                       FromId = x.ApplicationUserId,
                       ToId = x.ApplicationUserIdTo,
                       FromName = x.ApplicationUser.UserName,
                       ToName = x.ApplicationUserTo.UserName,
                       Time = x.Time,
                       Amount = x.Amount,
                       Succeed = x.Succeed
                   }).
                ToArray();
        }
    }
}
