using System;

namespace TSZ
{
    public class WeatherForecast
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string Summary { get; set; }
    }



    public class TransactionList
    {
        public int TransactionId { get; set; }
        public string  FromId { get; set; }

        public string ToId { get; set; }

        public string FromName { get; set; }

        public string ToName { get; set; }

        public decimal Amount { get; set; }

        public bool Succeed { get; set; }

        public DateTime Time { get; set; }
    }


}
