﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;


namespace TSZ.Models
{

    public class Pay
    {
        public string username { get; set; }
        public decimal amount { get; set; }

    }

    public class ApplicationUser : IdentityUser
    {
        [Column(TypeName = "money")]
        public decimal Amount { get; set; }

        //  public virtual ICollection<Transaction> Transactions { get; set; }


    }

    public class Transaction
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TransactionId { get; set; }
        public string ApplicationUserId { get; set; }

        
        [Column(TypeName = "money")]
        public decimal Amount { get; set; }

        [Column(TypeName = "money")]
        public decimal Ballance { get; set; }
        public DateTime Time { get; set; }
        public string ApplicationUserIdTo { get; set; }
        public bool Succeed { get; set; }

        [ForeignKey("ApplicationUserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        [ForeignKey("ApplicationUserIdTo")]
        public ApplicationUser ApplicationUserTo { get; set; }
    }

}
