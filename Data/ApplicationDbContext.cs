﻿using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TSZ.Models;

namespace TSZ.Data
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
    {
        public ApplicationDbContext(
            DbContextOptions options,
            IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
        }

        public DbSet<Transaction> Transactions { get; set; }

        public Transaction Transfer(ApplicationUser from, ApplicationUser to, decimal amount)
        {
            ApplicationDbContext DbContext = this;
            using (var transaction = DbContext.Database.BeginTransaction())
            {

                try
                {
                    Transaction trans = new Transaction();
                    trans.ApplicationUser = from;
                    trans.ApplicationUserTo = to;
                    trans.Amount = amount;
                    trans.Time = DateTime.Now;
                    trans.Succeed = from.Amount >= amount;
                    if (trans.Succeed)
                    {
                        DbContext.Attach(from);
                        DbContext.Attach(to);
                        from.Amount -= amount;
                        to.Amount += amount;
                    }

                    trans.Ballance = from.Amount;

                    DbContext.Add(trans);
                    DbContext.SaveChanges();

                    transaction.Commit();

                    return trans;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return null;
                }
                finally
                {

                }
            }

        }

    }
}
